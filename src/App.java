import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Iterator;
import java.util.Scanner;

public class App {

	public static void main(String[] args) {
		System.out.println("Hello World");

		int numero = 10;
		float numeroDecimal = 1.5f;
		double numeroDecimal2 = 40.1;
		char umCaractere = 'a';
		boolean eVerdadeiro = false;
		String umaFrase = "Olar!";

		if (numero == 10) {
			System.out.println(numero);
		} else if (numero == 20) {
			System.out.println("Não vai entrar aqui");
		} else {
			System.out.println("Nem aqui");
		}

//		for (int i = 0; i < 5; i++) {
//			System.out.println(i);
//		}

//		int j = 0;
//		while (j < 5) {
//			System.out.println(j);
//			j++;
//		}

		int[] numeros = { 1, 2, 3 };
		int[] novosNumeros = new int[10];
		String[] palavras = { "celular", "batatas", "frango" };

		for (String palavra : palavras) {
			System.out.println(palavra);
		}

		System.out.println(numeros[2]);
		novosNumeros[5] = 30;

		System.out.println(umaFrase);

		Scanner scanner = new Scanner(System.in);
		// int numeroEscolhido = scanner.nextInt();
		// String dadosUsuario = scanner.nextLine();
		int numeroConvertido = 0;
//		try {
//			numeroConvertido = Integer.parseInt(dadosUsuario);
//			System.out.println(numeroConvertido + 1);
//		} catch (NumberFormatException e) {
//			System.out.println("Não foi possível converter o número");
//		}									

		// Cria e lança uma excessão
		// throw new Exception("Exemplo");
//		try (Scanner scanner = new Scanner(System.in)) {
//			String ls = scanner.nextLine();
//		}

		System.out.println("Fim");
		/////////////

//		String st1 = scanner.nextLine();
//		String st2 = scanner.nextLine();

//		System.out.println(st1.equals(st2));

		// Exemplo de uso de Math
		double sorteio = Math.random();
		sorteio = Math.ceil(sorteio * 6);
		System.out.println(sorteio);

		// Exemplo de gravação de arquivo
		Path path = Paths.get("/home/lenovo/aulas/dia4/exemplo.txt");
		String umaString = "Vou gravar mais uma linha";

		try {
			Files.write(path, umaString.getBytes());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

}
